import React, { createContext, useReducer} from 'react'
import AppReducer from './AppReducer'


const initialState = {
    appName : process.env.MIX_APP_NAME
}


 //Create Context
 export const GlobalContext = createContext(initialState)

 //Provider component
 export const GlobalProvider = ({ children }) => {
     const [state, dispatch] = useReducer(AppReducer, initialState)

     return (
     <GlobalContext.Provider value={{state, dispatch}}>
         { children }
     </GlobalContext.Provider>
     )
 }
