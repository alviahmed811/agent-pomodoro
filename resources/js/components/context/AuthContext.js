import React, { createContext, useReducer} from 'react'
import AuthReducer from './AuthReducer'


const initialState = {
    isAuthenticated : localStorage.getItem('token') != null ? true : false,
    user: null,
    access_token : localStorage.getItem('token') != null ? localStorage.getItem('token') : null,
    isLoading: false
}


 //Create Context
 export const AuthContext = createContext(initialState)

 //create Consumer
//  export const Consumer = AuthContext.Consumer;

 //Provider component
 export const AuthProvider = ({ children }) => {
     const [state, dispatch] = useReducer(AuthReducer, initialState)

     return (
     <AuthContext.Provider value={{state, dispatch}}>
         { children }
     </AuthContext.Provider>
     )
 }
