import {
    USER_LOADED,
    USER_LOADING,
    AUTH_ERROR,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT_SUCCESS,
    REGISTER_SUCCESS,
    REGISTER_FAIL,
} from '../actions/types';

export default (state,action) => {
    switch (action.type) {
        case USER_LOADING:
        return {
            ...state,
            isLoading: true,
        };
        case USER_LOADED:
        return {
            ...state,
            isAuthenticated: true,
            isLoading: false,
            user: action.payload.name,
        };
        case LOGIN_SUCCESS:
        case REGISTER_SUCCESS:
        window.localStorage.setItem('token', action.payload.access_token);
        return {
            ...state,
            ...action.payload,
            isAuthenticated: true,
            isLoading: false,
        };
        case AUTH_ERROR:
        case LOGIN_FAIL:
        case LOGOUT_SUCCESS:
        case REGISTER_FAIL:
        window.localStorage.removeItem('token');
        return {
            ...state,
            token: null,
            user: null,
            isAuthenticated: false,
            isLoading: false,
        };
    }
}
