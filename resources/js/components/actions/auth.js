import axios from 'axios'
import { returnErrors } from './messages'

import {
    USER_LOADED,
    USER_LOADING,
    AUTH_ERROR,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT_SUCCESS,
    REGISTER_SUCCESS,
    REGISTER_FAIL,
} from './types';

// CHECK TOKEN & LOAD USER
export const loadUser = (dispatch, state) => {
    // User Loading
    dispatch({ type: USER_LOADING });

    axios
    .post('/api/auth/me', null,tokenConfig(state))
    .then((res) => {
        dispatch({
            type: USER_LOADED,
            payload: res.data,
        });
    })
    .catch((err) => {
        dispatch(returnErrors(err.response.data, err.response.status));
        dispatch({
            type: AUTH_ERROR,
        });
    });
};

// LOGIN USER
export const login = (email, password) => (dispatch) => {

    // Headers
    const config = {
        headers: {
            'Content-Type': 'application/json',
        },
    };

    // Request Body
    const body = JSON.stringify({ email, password });

    axios
    .post('/api/auth/login', body, config)
    .then((res) => {
        dispatch({
            type: LOGIN_SUCCESS,
            payload: res.data,
        });
    })
    .catch((err) => {
        dispatch(returnErrors(err.response.data, err.response.status));
        dispatch({
            type: LOGIN_FAIL,
        });
    });
};

// LOGOUT USER
export const logout = (dispatch, state) => {
    axios
    .post('/api/auth/logout/', null, tokenConfig(state))
    .then((res) => {
        // dispatch({ type: 'CLEAR_LEADS' });
        dispatch({
            type: LOGOUT_SUCCESS,
        });
    })
    .catch((err) => {
        dispatch(returnErrors(err.response.data, err.response.status));
    });
};

// Setup config with token - helper function
export const tokenConfig = (state) => {
    // Get token from state
    const token = state.access_token;

    // Headers
    const config = {
        headers: {
            'Content-Type': 'application/json',
        },
    };

    // If token, add to headers config
    if (token) {
        config.headers['Authorization'] = `Bearer ${token}`;
    }

    return config;
};
