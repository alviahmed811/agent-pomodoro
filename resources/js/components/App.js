import React, { Component, Fragment } from 'react';
import ReactDOM from "react-dom";
import { HashRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import PrivateRoute from './common/PrivateRoute'

import { AuthProvider } from './context/AuthContext'
import { GlobalProvider } from './context/GlobalContext'

import Navbar from './layouts/Navbar'
import Welcome from './Welcome'
import Login from './account/Login'
import Home from './Home';

function App(){

    return (
        <GlobalProvider>
            <AuthProvider>
                <Router>
                    <Fragment>
                        <Navbar/>
                        <div className="container">
                            <Switch>
                                <PrivateRoute exact path="/home" component={Home} />
                                <Route exact path="/" component={Welcome} />
                                <Route exact path="/login" component={Login} />
                            </Switch>
                        </div>
                    </Fragment>
                </Router>
            </AuthProvider>
        </GlobalProvider>
    )
}

export default App;

ReactDOM.render(<App/>, document.getElementById('app'))
