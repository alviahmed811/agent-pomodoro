import React, {useContext, useState } from 'react'
import { AuthContext } from '../context/AuthContext'
import { Link, Redirect } from 'react-router-dom';
import {login} from '../actions/auth'

function Login(){

    const {dispatch, state} = useContext(AuthContext)

    const initialState = {
        email: "",
        password: ""
    }

    const [data, setdata] = useState(initialState)

    const onChange = e => {
        setdata({
            ...data,
            [e.target.name]: e.target.value
        });
    }

    const onSubmit = (e) => {
        e.preventDefault();
        login(data.email, data.password)(dispatch);
    }

    // const context = useContext(AuthContext)
    if (state.isAuthenticated) {
        return <Redirect to="/home" />;
    }
    return (
        <div className="col-md-6 m-auto">
        <div className="card card-body mt-5">
          <h2 className="text-center">Login</h2>
          <form onSubmit={onSubmit}>
            <div className="form-group">
              <label>Username</label>
              <input
                type="text"
                className="form-control"
                name="email"
                onChange={onChange}
                value={data.email}
              />
            </div>

            <div className="form-group">
              <label>Password</label>
              <input
                type="password"
                className="form-control"
                name="password"
                onChange={onChange}
                value={data.password}
              />
            </div>

            <div className="form-group">
              <button type="submit" className="btn btn-primary">
                Login
              </button>
            </div>
            <p>
              Don't have an account? <Link to="/register">Register</Link>
            </p>
          </form>
        </div>
      </div>
    )
}

export default Login;

