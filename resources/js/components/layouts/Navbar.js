import React, { useContext,Component } from 'react';
import { AuthContext } from '../context/AuthContext'
import {Link, Redirect } from 'react-router-dom'
import { logout, loadUser } from '../actions/auth';

// const context = useContext(AuthContext)

// const GuestLinks= (props) => (
//     <ul className="navbar-nav nav-pills nav-fill ml-auto">
//         <li className="nav-item active">
//             <Link className="nav-link" to="/">Home <span className="sr-only">(current)</span></Link>
//         </li>
//         <li className="nav-item">
//             <Link className="nav-link" to="/login">Login</Link>
//         </li>
//     </ul>
//     )

    // const AuthLinks = (props) =>(
    //     <ul className="navbar-nav nav-pills nav-fill ml-auto">
    //         <span className="navbar-text mr-3">
    //             <strong>{props.user ? `Welcome ${props.user}` : 'Profile'}</strong>
    //         </span>
    //         <li className="nav-item">
    //             <button onClick={props.onClick} className="nav-link btn btn-info btn-sm text-light">
    //                 Logout
    //             </button>
    //         </li>
    //     </ul>
    // )

export default class Navbar extends Component{

    static contextType = AuthContext

    constructor(props)
    {
        super(props)
        this.state = {
            user: null,
            isAuthenticated: false
        }
        // this.willmount()
        // console.log(this.context.state)

    }

    willmount()
    {
        const {state, dispatch} = this.context


        if((!state.isLoading) && state.access_token!= null && state.user == null)
        {
            loadUser(dispatch, state)
            console.log(state)
            const authState = state
            this.setState((state, props) => {user: authState.user})
            this.setState((state, props) => {isAuthenticated: authState.isAuthenticated})
        }

    }

    componentDidMount()
    {
        const {state, dispatch} = this.context


        if((!state.isLoading) && state.access_token!= null && state.user == null)
        {
            loadUser(dispatch, state)
            const authState = state
            console.log(authState)
            this.setState((state, props) => {user: authState.user})
            this.setState((state, props) => {isAuthenticated: authState.isAuthenticated})
        }
    }

    // useEffect(() => {

    //     if((!state.isLoading) && state.access_token!= null && state.user == null)
    //     {
    //         loadUser(dispatch, state)
    //     }

    //     return () => {
    //         if(data.isAuthenticated != state.isAuthenticated)
    //     {
    //         setdata({
    //             ...data,
    //             isAuthenticated: state.isAuthenticated
    //         });
    //     }
    //     }
    // })

    onClick(e) {
        e.preventDefault()

        console.log(this)
        const {state, dispatch} = this.context

        logout(dispatch, state)
    }

    render(){

        const authLinks = (
            <ul className="navbar-nav nav-pills nav-fill ml-auto">
                <span className="navbar-text mr-3">
                    <strong>{this.state.user ? `Welcome ${this.state.user}` : 'Profile'}</strong>
                </span>
                <li className="nav-item">
                    <button onClick={this.onClick} className="nav-link btn btn-info btn-sm text-light">
                        Logout
                    </button>
                </li>
            </ul>
        )

        const guestLinks= (
            <ul className="navbar-nav nav-pills nav-fill ml-auto">
                <li className="nav-item active">
                    <Link className="nav-link" to="/">Home <span className="sr-only">(current)</span></Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/login">Login</Link>
                </li>
            </ul>
            )
        return (
            <nav className="navbar fixed-bottom navbar-expand-sm navbar-dark bg-info">
                <div className="container">
                    <a className="navbar-brand" href="/">
                        Agent Pomodoro
                    </a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarColor02">
                        {this.state.isAuthenticated ? authLinks : guestLinks}
                    </div>
                </div>
            </nav>
            );
    }
    }
