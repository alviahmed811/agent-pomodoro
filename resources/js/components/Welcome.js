import React, { useContext } from 'react'
import { GlobalContext } from './context/GlobalContext'

function Welcome() {
    const context = useContext(GlobalContext)
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8 border-info">
                    <h2>Greetings Agent</h2>
                </div>
                <div className="col-md-8">
                    <div className="card border-info">
                        <div className="card-header">Welcome to {context.state.appName}</div>
                        <div className="card-body">
                            You are one the choosen one the get selected in the prestigious agency, the hogward school of life. Your mission should you choose to accept it will be to
                            guide you life, be the master of your own will and track.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Welcome;
